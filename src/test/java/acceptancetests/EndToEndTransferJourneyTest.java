package acceptancetests;

import com.alisahin.moneytransfer.AppServer;
import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.accounts.domain.Owner;
import com.alisahin.moneytransfer.services.accounts.resources.AccountsResource;
import com.alisahin.moneytransfer.services.transfer.domain.Receipt;
import com.alisahin.moneytransfer.services.transfer.domain.TransferRequest;
import com.alisahin.moneytransfer.services.transfer.resources.TransferResource;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;

import static javax.ws.rs.client.Entity.entity;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class EndToEndTransferJourneyTest {

    public static final BigDecimal AMOUNT_25 = new BigDecimal("25");
    private static AppServer appServer;

    @BeforeClass
    public static void setUp() throws Exception {
        appServer = new AppServer();
        appServer.start(7779);
    }

    @AfterClass
    public static void clean() throws Exception {
        appServer.stop();
    }

    @Test
    public void shouldCreate2AccountsAndTransferMoney() throws Exception {
        String accountLocation1 = whenAnAccountIsCreatedWithBalance200AndAccountLocationIsRetrieved(new Owner("ali", "sahin"));
        String accountLocation2 = whenAnAccountIsCreatedWithBalance200AndAccountLocationIsRetrieved(new Owner("foo", "bar"));

        Account account1 = thenWeRetrieveTheAccountDetailsOf(accountLocation1);
        Account account2 = thenWeRetrieveTheAccountDetailsOf(accountLocation2);

        assertThat(account1.getOwner(), is(new Owner("ali", "sahin")));
        assertThat(account1.getBalance(), is(new BigDecimal("200")));

        Link link = thenWeGetTheTransferLinkForThe(accountLocation1);

        Receipt receipt = whenWePostATransferRequestFor(link, account1, account2, AMOUNT_25);

        assertThat(receipt, is(new Receipt(account2.getAccountId(), AMOUNT_25)));

        account1 = thenWeRetrieveTheAccountDetailsOf(accountLocation1);
        account2 = thenWeRetrieveTheAccountDetailsOf(accountLocation2);

        assertThat(account1.getBalance(), is(new BigDecimal("175")));
        assertThat(account2.getBalance(), is(new BigDecimal("225")));

    }

    private Receipt whenWePostATransferRequestFor(Link link, Account from, Account to, BigDecimal amount) {
        return ClientBuilder.newClient().target(link).
                request().post(entity(new TransferRequest(from.getAccountId(), to.getAccountId(), amount),
                TransferResource.APPLICATION_VND_MONEYSTRANSFER_TRANSFER_JSON)).readEntity(Receipt.class);

    }

    private Link thenWeGetTheTransferLinkForThe(String accountLocation) {
        return ClientBuilder.newClient().target(accountLocation).
                request().get().getLink("transfer");
    }

    private Account thenWeRetrieveTheAccountDetailsOf(String accountLocation) {
        return ClientBuilder.newClient().target(accountLocation).
                request().get().readEntity(Account.class);
    }

    private String whenAnAccountIsCreatedWithBalance200AndAccountLocationIsRetrieved(Owner entity) {
        Response response = ClientBuilder.newClient().target("http://localhost:7779/accounts").
                request().post(entity(entity, AccountsResource.APPLICATION_VND_MONEYSTRANSFER_NEWACCOUNT_JSON));
        return response.getHeaderString("Location");
    }
}
