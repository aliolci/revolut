package com.alisahin.moneytransfer.system.repository;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.accounts.domain.Owner;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class InMemoryAccountsRepositoryTest {

    private InThreadExceptionExtractor exceptionExtractor = new InThreadExceptionExtractor();

    @Test
    public void shouldNotThrowConcurrentModificationException() throws Exception {
        InMemoryAccountsRepository repo = new InMemoryAccountsRepository();
        loadRepoInAThread(repo);
        loadRepoInAThread(repo);
        waitForInitialLoad();
        concurrentReadingOnTheRepository(repo, exceptionExtractor);

        assertThat(exceptionExtractor.getException(), is(nullValue()));

    }

    private void concurrentReadingOnTheRepository(InMemoryAccountsRepository repo, InThreadExceptionExtractor exceptionExtractor) {
        new Thread(() -> {
            try {
                for (Account account : repo.allAccounts()) {
                }
            } catch (Exception e){
                exceptionExtractor.setException(e);
            }
        }).start();

    }

    private void waitForInitialLoad() throws InterruptedException {
        Thread.sleep(100);
    }

    private void loadRepoInAThread(InMemoryAccountsRepository repo) {
        new Thread(() -> {
            for (int i = 0; i < 750000; i++) {
                repo.addAccount(account(Integer.toString(i)));
            }
        }).start();
    }

    private static class InThreadExceptionExtractor {
        private Exception e;

        void setException(Exception e){
            this.e = e;
        }

        public Exception getException() {
            return e;
        }
    }

    private Account account(String val) {
        return new Account(new Owner("ali", "sahin"), val, new BigDecimal(val));
    }
}