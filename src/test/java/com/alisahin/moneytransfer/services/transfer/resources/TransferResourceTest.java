package com.alisahin.moneytransfer.services.transfer.resources;

import com.alisahin.moneytransfer.services.transfer.domain.Receipt;
import com.alisahin.moneytransfer.services.transfer.domain.TransferError;
import com.alisahin.moneytransfer.services.transfer.domain.TransferRequest;
import com.alisahin.moneytransfer.services.transfer.domain.TransferResult;
import com.alisahin.moneytransfer.services.transfer.service.TransferService;
import org.glassfish.jersey.uri.internal.JerseyUriBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TransferResourceTest {

    private static final TransferRequest REQUEST = new TransferRequest();
    private static final Receipt RECEIPT = new Receipt();
    private static final TransferError TRANSFER_ERROR = TransferError.NOT_ENOUGH_BALANCE;
    private TransferService transferService = mock(TransferService.class);
    private UriInfo uriInfo = mock(UriInfo.class);
    private TransferResource transferResource = new TransferResource(transferService, uriInfo);

    @Before
    public void setUp() throws Exception {
        when(uriInfo.getBaseUriBuilder()).thenReturn(stubUriBuilder());
    }

    @Test
    public void shouldReturnTransferReceiptWhenTransferServiceReturnsSuccessfully() throws Exception {
        when(transferService.transfer(REQUEST)).thenReturn(TransferResult.success(RECEIPT));
        Response response = transferResource.transfer(REQUEST);

        assertThat(response.getStatus(), is(200));
        assertThat(response.getEntity(), is(RECEIPT));
    }

    @Test
    public void shouldReturnTransferErrorWhenTransferServiceReturnsError() throws Exception {
        when(transferService.transfer(REQUEST)).thenReturn(TransferResult.failure(TRANSFER_ERROR));
        Response response = transferResource.transfer(REQUEST);

        assertThat(response.getStatus(), is(500));
        assertThat(response.getEntity(), is(TRANSFER_ERROR));
    }

    private JerseyUriBuilder stubUriBuilder() {
        return new JerseyUriBuilder(){
            @Override
            public JerseyUriBuilder path(String path) {
                return this;
            }
        };
    }
}