package com.alisahin.moneytransfer.services.transfer.service;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.transfer.domain.Receipt;
import com.alisahin.moneytransfer.services.transfer.domain.TransferError;
import com.alisahin.moneytransfer.services.transfer.domain.TransferRequest;
import com.alisahin.moneytransfer.services.transfer.domain.TransferResult;
import com.alisahin.moneytransfer.system.repository.AccountsRepository;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InternalAccountTransferServiceTest {

    public static final String FROM_ACCOUNT_ID = "fromId";
    public static final String TO_ACCOUNT_ID = "toId";
    public static final BigDecimal AMOUNT_0 = new BigDecimal("0");
    public static final BigDecimal AMOUNT_NEGATIVE = new BigDecimal("-1");
    public static final BigDecimal AMOUNT_3 = new BigDecimal("3");
    public static final BigDecimal AMOUNT_50 = new BigDecimal("50");
    public static final TransferRequest TRANSFER_REQUEST_FOR_AMOUNT_3 = new TransferRequest(FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT_3);
    public static final TransferRequest TRANSFER_REQUEST_FOR_AMOUNT_50 = new TransferRequest(FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT_50);
    public static final TransferRequest TRANSFER_REQUEST_FOR_AMOUNT_0 = new TransferRequest(FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT_0);
    public static final TransferRequest TRANSFER_REQUEST_FOR_AMOUNT_NEGATIVE = new TransferRequest(FROM_ACCOUNT_ID, TO_ACCOUNT_ID, AMOUNT_NEGATIVE);
    public static final Optional<Account> ACCOUNT_WITH_BALANCE_10 = Optional.of(new Account(null, FROM_ACCOUNT_ID, new BigDecimal("10")));
    public static final Optional<Account> ACCOUNT_WITH_BALANCE_5 = Optional.of(new Account(null, TO_ACCOUNT_ID, new BigDecimal("5")));
    public static final BigDecimal BALANCE_8 = new BigDecimal("8");
    public static final BigDecimal BALANCE_7 = new BigDecimal("7");
    private AccountsRepository accountsRepository = mock(AccountsRepository.class);
    private InternalAccountTransferService service = new InternalAccountTransferService(accountsRepository);

    @Test
    public void shouldTransferWhenThereIsEnoughBalanceInTheFromAccount() throws Exception {
        when(accountsRepository.findAccount(FROM_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_10);
        when(accountsRepository.findAccount(TO_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_5);

        TransferResult transfer = service.transfer(TRANSFER_REQUEST_FOR_AMOUNT_3);

        assertTrue(transfer.isSuccessful());
        assertThat(transfer.getReceipt().get(), is(new Receipt(TO_ACCOUNT_ID, AMOUNT_3)));
        assertThat(ACCOUNT_WITH_BALANCE_5.get().getBalance(), is(BALANCE_8));
        assertThat(ACCOUNT_WITH_BALANCE_10.get().getBalance(), is(BALANCE_7));
    }

    @Test
    public void shouldFailTransferWhenThereIsNoEnoughFundsInTheFromAccount() throws Exception {
        when(accountsRepository.findAccount(FROM_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_10);
        when(accountsRepository.findAccount(TO_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_5);

        TransferResult transfer = service.transfer(TRANSFER_REQUEST_FOR_AMOUNT_50);

        assertFalse(transfer.isSuccessful());
        assertThat(transfer.getError().get(), is(TransferError.NOT_ENOUGH_BALANCE));
    }

    @Test
    public void shouldFailTransferWhenFromAccountDoesNotFound() throws Exception {
        when(accountsRepository.findAccount(FROM_ACCOUNT_ID)).thenReturn(Optional.empty());
        when(accountsRepository.findAccount(TO_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_5);

        TransferResult transfer = service.transfer(TRANSFER_REQUEST_FOR_AMOUNT_3);

        assertFalse(transfer.isSuccessful());
        assertThat(transfer.getError().get(), is(TransferError.FROM_ACCOUNT_ID_CANNOT_BE_FOUND));
    }

    @Test
    public void shouldFailTransferWhenToAccountDoesNotFound() throws Exception {
        when(accountsRepository.findAccount(FROM_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_10);
        when(accountsRepository.findAccount(TO_ACCOUNT_ID)).thenReturn(Optional.empty());

        TransferResult transfer = service.transfer(TRANSFER_REQUEST_FOR_AMOUNT_3);

        assertFalse(transfer.isSuccessful());
        assertThat(transfer.getError().get(), is(TransferError.TO_ACCOUNT_ID_CANNOT_BE_FOUND));
    }

    @Test
    public void shouldFailTransferWhenTheAmountIsEqualTo0() throws Exception {
        when(accountsRepository.findAccount(FROM_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_10);
        when(accountsRepository.findAccount(TO_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_5);

        TransferResult transfer = service.transfer(TRANSFER_REQUEST_FOR_AMOUNT_0);

        assertFalse(transfer.isSuccessful());
        assertThat(transfer.getError().get(), is(TransferError.AMOUNT_SHOULD_BE_GREATER_THAN_0));

    }

    @Test
    public void shouldFailTransferWhenTheAmountIsLessThan0() throws Exception {
        when(accountsRepository.findAccount(FROM_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_10);
        when(accountsRepository.findAccount(TO_ACCOUNT_ID)).thenReturn(ACCOUNT_WITH_BALANCE_5);

        TransferResult transfer = service.transfer(TRANSFER_REQUEST_FOR_AMOUNT_NEGATIVE);

        assertFalse(transfer.isSuccessful());
        assertThat(transfer.getError().get(), is(TransferError.AMOUNT_SHOULD_BE_GREATER_THAN_0));

    }
}