package com.alisahin.moneytransfer.services.accounts.resources;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.accounts.domain.Owner;
import com.alisahin.moneytransfer.services.accounts.service.AccountService;
import org.glassfish.jersey.uri.internal.JerseyUriBuilder;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountsResourceTest {

    private static final String ACCOUNT_ID = "accountId";
    private static final Account ACCOUNT = new Account(new Owner("ali", "sahin"), ACCOUNT_ID, new BigDecimal("100"));
    private AccountService accountService = mock(AccountService.class);
    private UriInfo uriInfo = mock(UriInfo.class);
    private AccountsResource accountsResource = new AccountsResource(accountService, uriInfo);

    @Before
    public void setUp() throws Exception {
        when(uriInfo.getBaseUriBuilder()).thenReturn(stubUriBuilder());

    }

    @Test
    public void shouldReturnAccountDetails() throws Exception {
        when(accountService.getAccount(ACCOUNT_ID)).thenReturn(Optional.of(ACCOUNT));
        Response response = accountsResource.details(ACCOUNT_ID);

        assertThat(response.getStatus(), is(200));
        assertThat(response.getEntity(), is(ACCOUNT));
    }

    @Test
    public void shouldReturnNotFoundWhenAccountDoesNotExist() throws Exception {
        when(accountService.getAccount(ACCOUNT_ID)).thenReturn(Optional.empty());
        Response response = accountsResource.details(ACCOUNT_ID);

        assertThat(response.getStatus(), is(404));
    }

    private JerseyUriBuilder stubUriBuilder() {
        return new JerseyUriBuilder(){
            @Override
            public JerseyUriBuilder path(String path) {
                return this;
            }
        };
    }
}