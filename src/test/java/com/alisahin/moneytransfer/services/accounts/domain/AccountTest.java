package com.alisahin.moneytransfer.services.accounts.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static java.lang.Thread.sleep;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class AccountTest {

    private static final Owner OWNER = null;
    private static final String ACCOUNT_ID = "id";
    private static final BigDecimal BALANCE_3 = new BigDecimal("3");
    private static final BigDecimal BALANCE_5 = new BigDecimal("5");
    private static final BigDecimal BALANCE_0 = new BigDecimal("0");
    private Account account = new Account(OWNER, ACCOUNT_ID, BALANCE_5);

    @Test
    public void shouldLetTheSameThreadModifyTheAccount() throws Exception {
        account.lock();
        BigDecimal initialBalance = account.getBalance();
        account.decreaseBalance(BALANCE_5);
        BigDecimal decreasedBalance = account.getBalance();
        account.increaseBalance(BALANCE_3);
        BigDecimal increasedBalance = account.getBalance();
        account.unlock();
        assertThat(initialBalance, is(BALANCE_5));
        assertThat(decreasedBalance, is(BALANCE_0));
        assertThat(increasedBalance, is(BALANCE_3));
    }

    @Test
    public void shouldNotLetAccountBalanceBeModifiedWhenAnotherThreadIsReadingFromTheSameAccount() throws Exception {
        BigDecimal balance = mainThreadLockTheAccountAndRetrievesTheBalance();
        otherTreadTriesToDecreaseTheWholeAmountWhileTheMainThreadAcquireTheBalance();

        assertThat(balance, is(BALANCE_5));

        mainThreadUnlocksTheAccountAndLetTheOtherThreadModifyTheAccount();
        notificationDelay();
        assertThat(mainThreadLockTheAccountAndRetrievesTheBalance(), is(BALANCE_0));
    }

    private void mainThreadUnlocksTheAccountAndLetTheOtherThreadModifyTheAccount() {
        account.unlock();
    }

    private BigDecimal mainThreadLockTheAccountAndRetrievesTheBalance() throws InterruptedException {
        account.lock();
        return account.getBalance();
    }

    private void otherTreadTriesToDecreaseTheWholeAmountWhileTheMainThreadAcquireTheBalance() throws InterruptedException {
        new Thread(() -> {
            account.lock();
            account.decreaseBalance(BALANCE_5);
            account.unlock();
        }, "Other Thread").start();
        notificationDelay();
    }

    private void notificationDelay() throws InterruptedException {
        sleep(500);
    }
}