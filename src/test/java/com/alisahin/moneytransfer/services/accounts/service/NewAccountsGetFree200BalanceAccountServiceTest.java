package com.alisahin.moneytransfer.services.accounts.service;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.accounts.domain.Owner;
import com.alisahin.moneytransfer.system.repository.AccountsRepository;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewAccountsGetFree200BalanceAccountServiceTest {

    public static final BigDecimal BALANCE_200 = new BigDecimal("200");
    private AccountsRepository accountRepository = mock(AccountsRepository.class);
    private NewAccountsGetFree200BalanceAccountService service = new NewAccountsGetFree200BalanceAccountService(accountRepository);
    private Collection accounts = mock(Collection.class);

    @Test
    public void shouldAddNewAccountWithAccountIdBasedOnTheCurrentAccountsSize() throws Exception {
        accountRepositoryHas8AccountsAlready();
        Owner owner = new Owner("ali", "sahin");
        service.newAccount(owner);
        verify(accountRepository).addAccount(new Account(owner, "8", BALANCE_200));

    }

    @SuppressWarnings("unchecked")
    private void accountRepositoryHas8AccountsAlready() {
        when(accounts.size()).thenReturn(8);
        when(accountRepository.allAccounts()).thenReturn(accounts);
    }
}