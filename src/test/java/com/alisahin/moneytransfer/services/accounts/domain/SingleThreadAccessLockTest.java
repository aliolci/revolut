package com.alisahin.moneytransfer.services.accounts.domain;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SingleThreadAccessLockTest {

    public static final long LOCK_HOLDER_THREADS_HOLDS_LOCK_FOR_1_SECOND = 1000;
    private final SingleThreadAccessLock lock = new SingleThreadAccessLock();

    @Test
    public void shouldBlockTheOtherThreadsUntilTheLockHolderUnlocks() throws Exception {
        lockHolderThread().start();
        Thread blockedThread = blockedThread();
        blockedThread.start();
        waitForThreadStart();
        Thread.State blockedThreadInitialState = blockedThread.getState();
        assertThat(blockedThreadInitialState, is(Thread.State.WAITING));
    }

    @Test
    public void onlyTheLockHolderShouldBeAbleToUnlockTheLock() throws Exception {
        lock.lock();
        Thread blockedThread = blockedThreadThatTriesToUnlockTheLockWhichIsNotLockedByItself();
        blockedThread.start();
        waitForThreadStart();
        Thread.State blockedThreadInitialState = blockedThread.getState();
        assertThat(blockedThreadInitialState, is(Thread.State.WAITING));
        lock.unlock();
        waitForThreadStart();
        blockedThreadInitialState = blockedThread.getState();
        assertThat(blockedThreadInitialState, is(Thread.State.TERMINATED));
    }

    @Test
    public void sameThreadShouldNotBeBlocked() throws Exception {
        lock.lock();
        lock.blockThreadsWithNoLock();//should not be hanging
    }

    private void waitForThreadStart() throws InterruptedException {
        Thread.sleep(500);
    }

    private Thread blockedThread() {
        return new Thread(() -> {
            lock.blockThreadsWithNoLock();
        });
    }
    private Thread blockedThreadThatTriesToUnlockTheLockWhichIsNotLockedByItself() {
        return new Thread(() -> {
            lock.unlock();
            lock.blockThreadsWithNoLock();
        });
    }

    private Thread lockHolderThread() {
        return new Thread(() -> {
            lock.lock();
            try {
                Thread.sleep(LOCK_HOLDER_THREADS_HOLDS_LOCK_FOR_1_SECOND);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.unlock();
        });
    }

}