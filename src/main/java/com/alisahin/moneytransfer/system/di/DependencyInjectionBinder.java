package com.alisahin.moneytransfer.system.di;

import com.alisahin.moneytransfer.services.accounts.service.AccountService;
import com.alisahin.moneytransfer.services.accounts.service.NewAccountsGetFree200BalanceAccountService;
import com.alisahin.moneytransfer.services.transfer.service.InternalAccountTransferService;
import com.alisahin.moneytransfer.services.transfer.service.TransferService;
import com.alisahin.moneytransfer.system.repository.AccountsRepository;
import com.alisahin.moneytransfer.system.repository.InMemoryAccountsRepository;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;

public class DependencyInjectionBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bind(InMemoryAccountsRepository.class).to(AccountsRepository.class).in(Singleton.class);
        bind(NewAccountsGetFree200BalanceAccountService.class).to(AccountService.class);
        bind(InternalAccountTransferService.class).to(TransferService.class);
    }
}
