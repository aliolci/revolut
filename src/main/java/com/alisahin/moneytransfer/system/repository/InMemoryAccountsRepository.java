package com.alisahin.moneytransfer.system.repository;

import com.alisahin.moneytransfer.services.accounts.domain.Account;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryAccountsRepository implements AccountsRepository {
    private Map<String, Account> accounts = new ConcurrentHashMap<>();

    @Override
    public Boolean addAccount(Account account) {
        accounts.put(account.getAccountId(), account);
        return true;
    }

    @Override
    public Optional<Account> findAccount(String accountId) {
        return Optional.ofNullable(accounts.get(accountId));
    }

    @Override
    public Collection<Account> allAccounts() {
        return accounts.values();
    }
}
