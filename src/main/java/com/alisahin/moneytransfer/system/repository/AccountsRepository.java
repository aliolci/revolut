package com.alisahin.moneytransfer.system.repository;

import com.alisahin.moneytransfer.services.accounts.domain.Account;

import java.util.Collection;
import java.util.Optional;

public interface AccountsRepository {
    Boolean addAccount(Account account);
    Optional<Account> findAccount(String accountId);
    Collection<Account> allAccounts();
}
