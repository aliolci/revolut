package com.alisahin.moneytransfer;

import com.alisahin.moneytransfer.services.accounts.resources.AccountsResource;
import com.alisahin.moneytransfer.services.transfer.resources.TransferResource;
import com.alisahin.moneytransfer.system.di.DependencyInjectionBinder;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;
import java.net.URISyntaxException;

import static java.lang.String.format;
import static org.glassfish.jersey.jetty.JettyHttpContainerFactory.createServer;

public class AppServer {

    private static Server server;

    public static void main(String[] args) throws Exception {
        new AppServer().start(7779);
    }

    public void start(int i) throws URISyntaxException {
        ResourceConfig config = new ResourceConfig(AccountsResource.class, TransferResource.class);
        config.register(new DependencyInjectionBinder());
        config.register(JacksonFeature.class);
        server = createServer(new URI(format("http://%s:%s", "localhost", i)), config);
    }

    public void stop() throws Exception{
        server.stop();
    }
}
