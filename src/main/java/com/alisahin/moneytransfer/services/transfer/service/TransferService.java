package com.alisahin.moneytransfer.services.transfer.service;

import com.alisahin.moneytransfer.services.transfer.domain.TransferRequest;
import com.alisahin.moneytransfer.services.transfer.domain.TransferResult;

public interface TransferService {
    TransferResult transfer(TransferRequest transferRequest);
}
