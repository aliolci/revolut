package com.alisahin.moneytransfer.services.transfer.service;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.transfer.domain.Receipt;
import com.alisahin.moneytransfer.services.transfer.domain.TransferError;
import com.alisahin.moneytransfer.services.transfer.domain.TransferRequest;
import com.alisahin.moneytransfer.services.transfer.domain.TransferResult;
import com.alisahin.moneytransfer.system.repository.AccountsRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Optional;

public class InternalAccountTransferService implements TransferService {

    private AccountsRepository accountsRepository;

    @Inject
    public InternalAccountTransferService(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public TransferResult transfer(TransferRequest transferRequest) {
        Optional<Account> from = accountsRepository.findAccount(transferRequest.getFromAccountId());
        Optional<Account> to = accountsRepository.findAccount(transferRequest.getToAccountId());
        BigDecimal amount = transferRequest.getAmount();
        Optional<TransferError> validationError = validation(from, to, amount);
        if (validationError.isPresent()) {
            return TransferResult.failure(validationError.get());
        } else {
            Account fromAccount = from.get();
            Account toAccount = to.get();
            try {
                fromAccount.lock();
                toAccount.lock();
                return makeTransfer(transferRequest, fromAccount, toAccount, amount);
            } finally {
                fromAccount.unlock();
                toAccount.unlock();
            }
        }
    }

    private TransferResult makeTransfer(TransferRequest transferRequest, Account from, Account to, BigDecimal amount) {
        if (from.getBalance().compareTo(amount) < 0) {
            return TransferResult.failure(TransferError.NOT_ENOUGH_BALANCE);
        }
        to.increaseBalance(amount);
        from.decreaseBalance(amount);
        return TransferResult.success(new Receipt(transferRequest.getToAccountId(), amount));
    }

    private Optional<TransferError> validation(Optional<Account> from, Optional<Account> to, BigDecimal amount) {
        if (!from.isPresent()) {
            return Optional.of(TransferError.FROM_ACCOUNT_ID_CANNOT_BE_FOUND);
        }
        if (!to.isPresent()) {
            return Optional.of(TransferError.TO_ACCOUNT_ID_CANNOT_BE_FOUND);
        }
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            return Optional.of(TransferError.AMOUNT_SHOULD_BE_GREATER_THAN_0);
        }
        return Optional.empty();
    }
}
