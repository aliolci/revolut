package com.alisahin.moneytransfer.services.transfer.domain;

import java.math.BigDecimal;
import java.util.Date;

public class Receipt {
    private String toAccountId;
    private Date timeStamp = new Date();
    private BigDecimal amount;

    public Receipt() {
        //jackson
    }

    public Receipt(String toAccountId, BigDecimal amount) {
        this.toAccountId = toAccountId;
        this.amount = amount;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Receipt receipt = (Receipt) o;

        if (toAccountId != null ? !toAccountId.equals(receipt.toAccountId) : receipt.toAccountId != null) return false;
        return !(amount != null ? !amount.equals(receipt.amount) : receipt.amount != null);

    }

    @Override
    public int hashCode() {
        int result = toAccountId != null ? toAccountId.hashCode() : 0;
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        return result;
    }
}
