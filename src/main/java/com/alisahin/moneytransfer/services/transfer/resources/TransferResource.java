package com.alisahin.moneytransfer.services.transfer.resources;

import com.alisahin.moneytransfer.services.accounts.resources.AccountsResource;
import com.alisahin.moneytransfer.services.transfer.domain.TransferRequest;
import com.alisahin.moneytransfer.services.transfer.domain.TransferResult;
import com.alisahin.moneytransfer.services.transfer.service.TransferService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import java.net.URI;

import static com.alisahin.moneytransfer.services.accounts.resources.AccountsResource.detailsLink;
import static javax.ws.rs.core.Link.fromUriBuilder;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.serverError;

@Path("/transfer/")
public class TransferResource {
    public static final String APPLICATION_VND_MONEYSTRANSFER_TRANSFER_JSON = "application/vnd.moneystransfer.transfer+json";
    public static final String APPLICATION_VND_MONEYSTRANSFER_RECEIPT_JSON = "application/vnd.moneystransfer.receipt+json";
    private TransferService transferService;
    private UriInfo uriInfo;

    @Inject
    public TransferResource(TransferService transferService, @Context UriInfo uriInfo) {
        this.transferService = transferService;
        this.uriInfo = uriInfo;
    }

    @POST
    @Consumes(APPLICATION_VND_MONEYSTRANSFER_TRANSFER_JSON)
    @Produces(APPLICATION_VND_MONEYSTRANSFER_RECEIPT_JSON)
    public Response transfer(TransferRequest request){
        TransferResult transfer = transferService.transfer(request);
        ResponseBuilder builder;
        if(transfer.isSuccessful()) {
            builder = ok(transfer.getReceipt().get());
        } else {
            builder =  serverError().entity(transfer.getError().get());
        }
        return builder.link(detailsLink(request.getFromAccountId(), uriInfo), "details").build();
    }

    public static URI transferLink(UriInfo uriInfo) {
        return fromUriBuilder(uriInfo.getBaseUriBuilder().path(TransferResource.class)).build().getUri();
    }
}
