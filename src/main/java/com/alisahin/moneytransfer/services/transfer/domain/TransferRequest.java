package com.alisahin.moneytransfer.services.transfer.domain;

import java.math.BigDecimal;

public class TransferRequest {
    private String fromAccountId;
    private String toAccountId;
    private BigDecimal amount;

    public TransferRequest() {
        //jackson
    }

    public TransferRequest(String fromAccountId, String toAccountId, BigDecimal amount) {
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.amount = amount;
    }

    public String getFromAccountId() {
        return fromAccountId;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
