package com.alisahin.moneytransfer.services.transfer.domain;

import java.util.Optional;

public class TransferResult {
    private final Optional<Receipt> receipt;
    private final Optional<TransferError> error;

    private TransferResult(Receipt receipt, TransferError error) {
        this.receipt = Optional.ofNullable(receipt);
        this.error = Optional.ofNullable(error);
    }

    public static TransferResult success(Receipt receipt){
        return new TransferResult(receipt, null);
    }

    public static TransferResult failure(TransferError transferError){
        return new TransferResult(null, transferError);
    }

    public Optional<Receipt> getReceipt() {
        return receipt;
    }

    public Optional<TransferError> getError() {
        return error;
    }

    public boolean isSuccessful() {
        return receipt.isPresent();
    }
}
