package com.alisahin.moneytransfer.services.transfer.domain;

public class TransferError {
    public static final TransferError NOT_ENOUGH_BALANCE = new TransferError("No enough balance to do this operation");
    public static final TransferError FROM_ACCOUNT_ID_CANNOT_BE_FOUND = new TransferError("From account id cannot be found");
    public static final TransferError TO_ACCOUNT_ID_CANNOT_BE_FOUND = new TransferError("To account id cannot be found");
    public static final TransferError AMOUNT_SHOULD_BE_GREATER_THAN_0 = new TransferError("The amount should be greater than 0");
    public static final TransferError INTERNAL_ERROR = new TransferError("Internal error");

    public String getErrorMessage() {
        return errorMessage;
    }

    private String errorMessage;

    private TransferError(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
