package com.alisahin.moneytransfer.services.accounts.domain;

public class Owner {
    private String name;
    private String surname;

    public Owner() {
        //jackson parser
    }

    public Owner(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Owner owner = (Owner) o;

        if (name != null ? !name.equals(owner.name) : owner.name != null) return false;
        return !(surname != null ? !surname.equals(owner.surname) : owner.surname != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }
}
