package com.alisahin.moneytransfer.services.accounts.service;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.accounts.domain.Owner;

import java.util.Collection;
import java.util.Optional;

public interface AccountService {
    Collection<Account> accounts();

    /**
    * returns account id
    * */
    String newAccount(Owner owner);

    Optional<Account> getAccount(String accountId);
}
