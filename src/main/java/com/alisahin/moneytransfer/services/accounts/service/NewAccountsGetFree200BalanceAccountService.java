package com.alisahin.moneytransfer.services.accounts.service;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.accounts.domain.Owner;
import com.alisahin.moneytransfer.system.repository.AccountsRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;

public class NewAccountsGetFree200BalanceAccountService implements AccountService {

    private AccountsRepository accountsRepository;

    @Inject
    public NewAccountsGetFree200BalanceAccountService(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    public Collection<Account> accounts() {
        return accountsRepository.allAccounts();
    }

    @Override
    public String newAccount(Owner owner) {
        String accountId = Integer.toString(accountsRepository.allAccounts().size());
        accountsRepository.addAccount(new Account(owner, accountId, new BigDecimal("200")));
        return accountId;
    }

    @Override
    public Optional<Account> getAccount(String accountId) {
        return accountsRepository.findAccount(accountId);
    }
}
