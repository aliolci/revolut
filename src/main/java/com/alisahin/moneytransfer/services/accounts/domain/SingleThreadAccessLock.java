package com.alisahin.moneytransfer.services.accounts.domain;

public class SingleThreadAccessLock {
    private volatile boolean locked = false;
    private volatile Thread lockHolder = null;

    public synchronized void lock() {
        if (locked) {
            blockThreadsWithNoLock();
        } else {
            this.locked = true;
            this.lockHolder = Thread.currentThread();
        }
    }

    public synchronized void unlock() {
        if(locked && Thread.currentThread().equals(lockHolder)) {
            this.locked = false;
            this.lockHolder = null;
            notifyAll();
        } else {
            blockThreadsWithNoLock();
        }
    }

    public synchronized void blockThreadsWithNoLock() {
        while (this.locked && !Thread.currentThread().equals(lockHolder)){
            try {
                wait();
            } catch (InterruptedException e) {
                //retry the lock
            }
        }
    }
}
