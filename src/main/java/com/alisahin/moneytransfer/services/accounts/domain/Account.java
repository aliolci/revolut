package com.alisahin.moneytransfer.services.accounts.domain;

import java.math.BigDecimal;

public class Account {
    private SingleThreadAccessLock lock = new SingleThreadAccessLock();
    private Owner owner;
    private String accountId;
    private volatile BigDecimal balance;

    public Account() {
        //jackson
    }

    public Account(Owner ownerName, String accountId, BigDecimal balance) {
        this.owner = ownerName;
        this.accountId = accountId;
        this.balance = balance;
    }

    public Owner getOwner() {
        return owner;
    }

    public String getAccountId() {
        return accountId;
    }

    public void lock(){
        this.lock.lock();
    }

    public void unlock(){
        this.lock.unlock();
    }

    public BigDecimal getBalance() {
        this.lock.blockThreadsWithNoLock();
        return balance;
    }

    public void increaseBalance(BigDecimal amount) {
        this.lock.blockThreadsWithNoLock();
        this.balance = balance.add(amount);
    }

    public void decreaseBalance(BigDecimal amount) {
        this.lock.blockThreadsWithNoLock();
        this.balance = balance.subtract(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (owner != null ? !owner.equals(account.owner) : account.owner != null) return false;
        if (accountId != null ? !accountId.equals(account.accountId) : account.accountId != null) return false;
        return !(balance != null ? !balance.equals(account.balance) : account.balance != null);

    }

    @Override
    public int hashCode() {
        int result = owner != null ? owner.hashCode() : 0;
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        return result;
    }
}
