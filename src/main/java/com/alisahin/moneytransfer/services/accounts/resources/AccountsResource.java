package com.alisahin.moneytransfer.services.accounts.resources;

import com.alisahin.moneytransfer.services.accounts.domain.Account;
import com.alisahin.moneytransfer.services.accounts.domain.Owner;
import com.alisahin.moneytransfer.services.accounts.service.AccountService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.alisahin.moneytransfer.services.transfer.resources.TransferResource.transferLink;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.Link.fromResource;
import static javax.ws.rs.core.Link.fromUriBuilder;
import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.ok;

@Path("/accounts/")
public class AccountsResource {
    public static final String APPLICATION_VND_MONEYSTRANSFER_NEWACCOUNT_JSON = "application/vnd.moneystransfer.newaccount+json";
    private AccountService accountService;
    private UriInfo uriInfo;

    @Inject
    public AccountsResource(AccountService accountService, @Context UriInfo uriInfo) {
        this.accountService = accountService;
        this.uriInfo = uriInfo;
    }

    @GET
    @Produces("application/vnd.moneytransfer.accounts+json")
    public Response accounts() {
        Collection<Account> accounts = accountService.accounts();
        ResponseBuilder builder = ok();
        return builder.entity(accounts.stream().map((account) -> accountBrief(account, builder)).collect(toList())).
                links(fromResource(AccountsResource.class).baseUri(uriInfo.getBaseUri()).rel("self").build()).build();
    }

    @GET
    @Produces("application/vnd.moneytransfer.accounts.details+json")
    @Path("{accountId}")
    public Response details(@PathParam("accountId") String accountId){
        return accountService.getAccount(accountId).
                map(account -> ok().entity(account).
                        link(detailsLink(account.getAccountId(), uriInfo), "self").
                        link(transferLink(uriInfo), "transfer")
                        .build()).
                orElse(Response.status(404).build());

    }

    @POST
    @Consumes(APPLICATION_VND_MONEYSTRANSFER_NEWACCOUNT_JSON)
    public Response newAccount(Owner owner){
        String accountId = accountService.newAccount(owner);
        return created(detailsLink(accountId, uriInfo)).build();
    }

    private Map<String, Object> accountBrief(Account account, ResponseBuilder builder) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("owner", account.getOwner());
        map.put("id", account.getAccountId());
        builder.links(fromResource(AccountsResource.class).baseUri(detailsLink(account.getAccountId(), uriInfo)).
                rel("details").
                title(account.getOwner().toString()).
                build());
        return map;
    }

    public static URI detailsLink(String accountId, UriInfo uriInfo) {
        return fromUriBuilder(uriInfo.getBaseUriBuilder().path(AccountsResource.class).path(accountId)).build().getUri();
    }


}
